=head1 Introduction

This document will introduce you to the SpaceBotWar server and the open API that will
allow you to interact with the server.

The API is also used by the SpaceBotWar User Interface (SBW UI)




=head2 SERVER

The playable servers are as follows:

  spacebotwar.com

If more servers are introduced they will be published here.






=head1 Web-Sockets

The Web Servers use predominatly Web Socket technology. Web Sockets offer significant
advantages over HTTP requests, even AJAX calls.

=over

=item *

The overhead for each request is much smaller making for a
faster response.

=item *

It is asynchronous and full-duplex. This means that the server can 'push' data to
all the clients at any time there is something to say. The client no longer has to
resort to constant polling or Long Polling methods.

=item *

By only supporting Web Sockets we can offer a significantly trimmed down Web
Server which is both faster and cheaper. We can then scale out horizontally to
provide more web servers for the same cost.

=back

The consequence however is that your client code needs to be a little bit more
sophisticated, however we have provided several examples of simple web servers
that you can build upon if you wish.





=head1 Example Calls

Here is an example connection to a Web Socket (in Perl)

    use AnyEvent::WebSocket::Client;
    my $client = AnyEvent::WebSocket::Client->new;

    my $connection;

    $client->connect("ws://spacebotwar.com/ws/game/lobby")->cb(sub {
        ....
    });

You can find a more complete example in the B<t> directory.





=head1 API hierarchy

The API call is split into separate components.

=over

=item Connection

Each Connection requires a separate Web Socket Connection. e.g. B<ws://spacebotwar.com/ws/chat> is
the connection to the chat system. There is also a B<game> and an B<arena> system. You may have a 
Web Socket connection into more than one of these at the same time.

=item Room

Each Connection can connect to one room. First you should join the B<Lobby> which will have 
limited functionality, but from which you may be allocated (or perhaps choose) which room to go
into. For example, if you go into the chat lobby, you may be able to choose from a list of 
available rooms (e.g. game_chat, help, alliance_chat etc.). Some Connections may only allow you
to go into one room at a time (e.g. arena).

=item Route

Every command has a unique B<route> using a path-like hierarchy to group commands with similar
functionality together. e.g. user account commands are B</user/register> B</user/login> and
B</user/logout>

=back

Putting all these together we have the following hierarchy.

    ws://spacebotwar.com/ws/game/zone1

    ws://spacebotwar.com/ws/game    => Connection
    zone1                           => Room

The Route is included in the JSON message which is sent to the Server (and sent back from it)
(see below)





=head1 Web Socket Message structure.

A web-socket call is simply a JSON encoded string.

  '{ "route" : "/user/register", "content" : { "username" : "james_bond", "password" : "topsecret" } }'

The B<route> describes the full path to the command to carry out. (in this case the B<User> command B<register>
API documentation will follow the same organization of the B<route>. e.g. this will be documented in the L<User.pod>

The B<content> is the actual payload of the message. In this example it defines the B<username> and the B<password>
required to register a new account.

We will use the same message format when sending messages from a route, back to the client.

A typical server response could be (as a string).

  '{ "room" : "zone_1", "route" : "/user/register_status", "content" : { "code" : "0", "message" : "Registered!" } }'

(Note the inclusion of the B<room>. It is not necessary to specify this in the Client call since it is implicit
as part of the conection.)

To help to distinguish between client or server messages, we will document the route as either.

  Client : register

To show that this is a message sent B<from> the Client.

or

  Server : register_confirm

For messages sent B<from> the Server.

e.g. for the above.




=head2 Client : register

Send a registration message from the Client to the server.

    {
        "username"  : "james_bond",
        "password"  : "tops3cret",
        "email"     : "agent007@mi5.gov.co.uk",
        "id"        : "james_bond",
    }

=head3 username (required)

The name you will use to log into the site. This must be at least 3 charaters long
and may consist of letters, numbers or punctuation.

=head3 password (required)

The password to authenticate you to the server. At least one upper-case character,
one lower-case character and either a numeric or punctuation character. The password
must also be at least 5 characters long.

=head3 email (required)

Your email address. You will not be able to log onto the server until your email
address has been verified. We will send a message with instructions on how to log
in to this email address, so make sure it is correct.

=head3 id (optional)

This allows you to give a unique ID to the message. For example if you were sending
many such requests. The server will add the id to any response so you can match them
up. An B<id> can be any string which can be unique, in this case the B<username>
makes a good B<id>. In other cases an incrementing number could be used.

=head3 RESPONSE

The server will send a B<Server : register> message with the success or failure
of this call.




=head2 Server : register

A response to a B<Client : register> message.

    {
        "code"      : "1000",
        "message"   : "That username already taken",
        "data"      : "james_bond",
        "id"        : "james_bond",
    }

or

    {
        "code"      : "0",
        "message"   : "Success.",
        "data"      : "james_bond",
        "id"        : "james_bond",
    }


=head3 code

A numeric value representing the status. B<0> is success, any other value is failure.

=head3 message

A human readable description of the status. This field should not be used programatically
since it may change. Use the B<code> field instead.

=head3 data

In some cases data will be returned

=head3 id

Where an B<id> was passed in the call, it is returned by the server.

=head3 RESPONSE

No Client response is nessesary.





=head1 Generic Server Status.

The 'formula' for the Server Status shown above is sufficiently common, and verbose
that we will create a shorthand for it.




=head2 Server : register_status

Returns B<code> B<message> B<data> and B<id>(see L<intro.pod>)

If you see this in any of the other documententaton you know it will have the same
structure as above.

